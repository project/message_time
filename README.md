# Message Time

## Table of contents

- Introduction
- Installation
- Requirements
- Configuration
- Maintainers


## Introduction

Adds an option to set message time delay, before it disappears.


## Installation

- Install via /admin/modules
- `composer require drupal/message_time`
- `drush en message_time -y`


## Requirements

This module requires no modules outside of Drupal core.


## Configuration

Goto: /admin/config/user-interface/message-time

Enter a numeric value in milliseconds in the "Message Time" field. Now the
message box will disappear after X seconds, as we hardcoded the duration
(10000ms = 10 sec)


## Maintainers

Supporting by:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
